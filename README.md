# Summary

The Postgres App Server role sets up the Postgres server

This role is used to setup the following:

* Installs the application server and configures the server to be bonded to a green (internal) IP

<!-- # Restrictions
* This docker role is designed to only have one instance per machine -->

## Additional instructions

To generate an MD5 password you can do the following in macOS

    echo "md5`echo -n "passwordUSERNAME" | md5`"

## Variables

| Variable name             | Description                                                    | Default        |
|---------------------------|----------------------------------------------------------------|----------------|
| postgres_container        |                                                                |                |
|   install_type            | Currently supports docker                                      | docker         |
|   name                    | Container name                                                 | postgres       |
|   version                 | Software version                                               | 17             |
|   root_password           | Password for the postgres user                                 | secretpassword |

| Variable name             | Description                                                    | Default        |
|---------------------------|----------------------------------------------------------------|----------------|
| postgres_infrastructure   |                                                                |                |
|   data_directory          | Directory for data files                                       |                |
|   config_directory        | Directory for configuration files                              |                |
|   backup_directory        | Directory for backup files                                     |                |
|   log_directory           | Directory for log files                                        |                |
|   external_port           | Port for access                                                | 5432           |

## Tasks

| Task name                 |                                                                | Parameters     |
|---------------------------|----------------------------------------------------------------|----------------|

## Legacy Variables that are passed into the role

| Variable name             | Description                                                    | Variable File stored |
|---------------------------|----------------------------------------------------------------|----------------------|
| postgres_install_type     | Select between server and docker install                       | group                |
| postgres_data_directory   | Postgres Data Directory                                        | group                |
| postgres_config_directory | Postgres Configuration Directory                               | group                |
| postgres_version          | Postgres Version                                               | group                |
| postgres_root_password    | Root password for Postgres                                     | group                |
| postgres_container_name   | Name of Postgres container                                     | group                |
| postgres_external_port    | External port for Postgres                                     | group                |

| Variable name           | Description                                                      | Variable File stored |
|-------------------------|------------------------------------------------------------------|----------------------|
| green_ip_address        | Internal IP address binding the Posgres server                   | host                 |
| pg_install_username     | Install user name to create application databases                | global               |
| pg_install_password     | Install user password to create application databases            | global               |
| app_database_server     | Database server which the app db is to be installed              | group                |
| app_database_name       | Database name which the app db is to be installed                | group                |
| app_database_username   | Username for the application database                            | group                |
| app_database_password   | Password for the application database user                       | group                |