---
  - name: Setup variables to be used in the database setup
    set_fact:
      app_database_name: "{{ app_database.name }}"
      app_database_username: "{{ app_database.user_name }}"
      app_database_password_encrypted: "{{ app_database.user_password }}"
      app_database_server: "{{ app_database.host_name }}"
      app_database_port: "{{ app_database.host_port }}"
      app_container_name: "{{ app_infrastructure.container_name }}"
      app_name: "{{ app_info.name }}"
      app_log_directory: "{{ app_infrastructure.log_directory }}"
      app_backup_directory: "{{ app_infrastructure.backup_directory }}"
      postgres_install_user_name: "{{ app_database.root_user }}"
      postgres_install_user_password: "{{ app_database.root_password }}"

  - name: Install postgres client binaries
    ansible.builtin.include_tasks: install-postgresql-client.yml

  - name: Setup database
    community.general.postgresql_db:
      name: "{{ app_database_name }}"
      login_host: "{{ app_database_server }}"
      login_user: "{{ postgres_install_user_name }}"
      login_password: "{{ postgres_install_user_password }}"
 
  - name: Setup user and privileges for the database
    community.general.postgresql_user:
      name: "{{ app_database_username }}"
      password: "{{ app_database_password_encrypted }}"
      db: "{{ app_database_name }}"
      state: present
      encrypted: true
      login_host: "{{ app_database_server }}"
      login_user: "{{ postgres_install_user_name }}"
      login_password: "{{ postgres_install_user_password }}"

  - name: Grant all privileges for the app database
    community.general.postgresql_privs:
      db: "{{ app_database_name }}"
      privs: ALL
      type: schema
      objs: public
      role: "{{ app_database_username }}"
      login_host: "{{ app_database_server }}"
      login_user: "{{ postgres_install_user_name }}"
      login_password: "{{ postgres_install_user_password }}"

  - name: Check if backup script exists
    stat:
      path: "/scripts/{{ database_backup_scripts_filename }}"
    register: database_backup_scripts

  - name: Create backup script for app database
    ansible.builtin.include_tasks: create-backup-script.yml
    when: not database_backup_scripts.stat.exists

  - name: Create backup job for app database
    ansible.builtin.include_tasks: create-backup-task.yml
    vars:
      database_name: "{{ app_database_name }}"
      container_name: "{{ app_container_name }}"
      application_name: "{{ app_name }}"
      database_host: "{{ app_database_server }}"
      database_port: "{{ app_database_port }}"
      app_dba_user: "{{ postgres_install_user_name }}"
      app_dba_password: "{{ postgres_install_user_password }}"